package com.example.practica01kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.widget.Button
import android.widget.EditText
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    private lateinit var btnSaludar: Button;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnCerrar: Button;
    private lateinit var txtSaludo: EditText;
    private lateinit var lblSaludo: TextView;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSaludar = findViewById(R.id.btnSaludar) as Button;
        btnLimpiar = findViewById(R.id.btnLimpiar) as Button;
        btnCerrar = findViewById(R.id.btnCerrar) as Button;
        txtSaludo = findViewById(R.id.txtSaludo) as EditText;
        lblSaludo = findViewById(R.id.lblSaludo) as TextView;

        btnSaludar.setOnClickListener {
            var str: String
            if (txtSaludo.text.toString().contentEquals("")) {
                Toast.makeText(applicationContext, "Falto capturar datos", Toast.LENGTH_LONG).show()
            }
            else {
                str = "Hola " + txtSaludo.text.toString() + " ¿Cómo estás?";
                lblSaludo.text = str;
            }
        }

        btnLimpiar.setOnClickListener {
            lblSaludo.text = ""
            txtSaludo.setText("").toString()
            txtSaludo.requestFocus()
        }

        btnCerrar.setOnClickListener {
            finish()
        }
    }
}
